/*!
 * project_shg
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2023. MIT licensed.
 */function fixDataContents() {
  $(".data_contents p span").css("font-size", "unset"), $(".data_contents div span").css("font-size", "unset"), $(".data_contents ul li").css("list-style", "unset"), $(".data_contents span").css("font-family", "unset"), $(".data_contents table").wrap('<div class="scrollMobiForTable"></div>'), $(".data_contents img").wrap('<p style="text-align:center;"></p>'), $(".data_contents img").removeAttr("style"), $(".data_contents table").removeAttr("style");
}
function setCustomValidationMessages() {
  jQuery.extend(jQuery.validator.messages, {
    required: "Vui l\xf2ng nh\u1eadp th\xf4ng tin b\u1eaft bu\u1ed9c"
  });
}
$(document).ready(function () {
  function e() {
    $(".fourColNumberBlock .smallBlock .textPart .text_1").each(function () {
      var e = $(this);
      jQuery({
        Counter: 0
      }).animate({
        Counter: e.attr("value")
      }, {
        duration: 2500,
        easing: "swing",
        step: function () {
          e.hasClass("pTagSmallBlock_3") ? e.text(Math.ceil(this.Counter) + "%") : (e.hasClass("pTagSmallBlock_1") || e.hasClass("pTagSmallBlock_2") || e.hasClass("pTagSmallBlock_4")) && e.text(Math.ceil(this.Counter) + "+");
        }
      });
    });
  }
  function o() {
    $(".fourColNumberBlock_2 .smallBlock .text_2").each(function () {
      var e = $(this);
      jQuery({
        Counter: 0
      }).animate({
        Counter: e.attr("value")
      }, {
        duration: 2500,
        easing: "swing",
        step: function () {
          e.hasClass("hasPercent") ? e.text(Math.ceil(this.Counter) + "%") : e.text("+" + Math.ceil(this.Counter));
        }
      });
    });
  }
  function n() {
    $(window).scroll(function () {
      var e = $(".menuFagoAgency_DESK .menuLine_1").height();
      if (t >= 992) {
        var o = e;
        $("#cateNewsFagoAgency .secMainContent .rightBlock .wrapSticky").css("top", o), $("#detailNewsFagoAgency .secMainContent .rightBlock .wrapSticky").css("top", o), $("#detailRecruitFagoAgency .secMainContent .rightBlock .wrapSticky").css("top", o);
      }
    });
  }
  var t = $(window).width();
  $(".owlMagazineBlock").owlCarousel({
    loop: !0,
    margin: 0,
    nav: !0,
    dots: !1,
    autoplay: !0,
    autoplayTimeout: 3e3,
    autoplayHoverPause: !0,
    smartSpeed: 1e3,
    responsive: {
      0: {
        items: 1,
        stagePadding: 20
      },
      767: {
        items: 2
      },
      991: {
        items: 3
      }
    }
  }), $(".owlMagazineBlock .owl-nav").attr("id", "owl-nav1"), $(".ourPartBlock .branchesBlock .branchPart").click(function () {
    $(".ourPartBlock .branchesBlock .branchPart").removeClass("active"), $(this).addClass("active");
    var e = $(this).text();
    "T\u1ea5t c\u1ea3" == e ? $(".ourPartBlock .logoBranchBlock .imgPart").fadeIn() : ($(".ourPartBlock .logoBranchBlock .imgPart").fadeOut(), $('.ourPartBlock .logoBranchBlock .imgPart[branch="' + e + '"]').fadeIn(600));
  });
  var a = 1;
  $(window).scroll(function () {
    var o = $(".fourColNumberBlock");
    if (o.length) {
      var n = $(".fourColNumberBlock").height(),
        t = $(window).height(),
        i = o.offset().top - t + n;
      $("html,body").scrollTop() >= i && 1 == a && (a = 2, e());
    }
  });
  var i = 1;
  $(window).scroll(function () {
    var e = $(".fourColNumberBlock_2");
    if (e.length) {
      var n = $(".fourColNumberBlock_2").outerHeight(),
        t = $(window).height(),
        a = e.offset().top - t + n;
      $("html,body").scrollTop() >= a && 1 == i && (i = 2, o());
    }
  }), setTimeout(() => {
    $(".slideServicesBlock .owlSlideServices").owlCarousel({
      loop: !0,
      margin: 0,
      nav: !0,
      dots: !1,
      autoplay: !0,
      autoplayTimeout: 4e3,
      autoplayHoverPause: !0,
      smartSpeed: 1e3,
      responsive: {
        0: {
          items: 2,
          slideBy: 1
        },
        991: {
          items: 4,
          slideBy: 2
        }
      }
    }), $(".slideServicesBlock .owlSlideServices .owl-nav").attr("id", "owl-nav1");
  }, 1e3), setTimeout(function () {
    $(".owlBlockNews_1").owlCarousel({
      loop: !0,
      margin: 10,
      nav: !0,
      dots: !1,
      smartSpeed: 1e3,
      responsive: {
        0: {
          items: 1,
          stagePadding: 30
        },
        575: {
          items: 2
        },
        991: {
          items: 3
        }
      }
    }), $(".owlBlockNews_1 .owl-nav").attr("id", "owl-nav1");
  }, 1e3), setTimeout(function () {
    $(".owlGalleryBlock").owlCarousel({
      loop: !0,
      margin: 10,
      nav: !0,
      dots: !1,
      autoplay: !0,
      autoplayTimeout: 4e3,
      autoplayHoverPause: !0,
      smartSpeed: 1e3,
      responsive: {
        0: {
          items: 1,
          slideBy: 1,
          stagePadding: 30
        },
        575: {
          items: 3,
          slideBy: 2
        },
        767: {
          items: 4,
          slideBy: 2
        }
      }
    }), $(".owlGalleryBlock .owl-nav").attr("id", "owl-nav1");
  }, 1e3), $(".wrap-founder .btnViewMorePeople").click(function () {
    var e = 0;
    $(this).parent().parent().find(".wrap-founder .col-fix.d-none").each(function () {
      if (!(e < 4)) return !1;
      $(".wrap-founder .col-fix.d-none").fadeIn(600), $(".wrap-founder .col-fix.d-none").removeClass("d-none"), e++;
    }), $(this).parent().find(".wrap-founder .col-fix.d-none").length || $(this).css("display", "none");
  }), $(".wrap-staff .btnViewMorePeople").click(function () {
    var e = 0;
    $(this).parent().parent().find(".wrap-staff .col-fix.d-none").each(function () {
      if (!(e < 4)) return !1;
      $(this).fadeIn(600), $(this).removeClass("d-none"), e++;
    }), $(this).parent().parent().find(".wrap-staff .col-fix.d-none").length || $(this).css("display", "none");
  }), setTimeout(function () {
    $(".owlStaffTeamBlock").owlCarousel({
      loop: !0,
      margin: 0,
      nav: !0,
      dots: !1,
      smartSpeed: 500,
      responsive: {
        0: {
          items: 1,
          slideBy: 1,
          stagePadding: 40
        },
        450: {
          items: 2,
          slideBy: 1
        },
        575: {
          items: 2,
          slideBy: 1
        },
        767: {
          items: 3,
          slideBy: 2
        },
        991: {
          items: 4,
          slideBy: 3
        }
      }
    }), $(".owlStaffTeamBlock .owl-nav").attr("id", "owl-nav1");
  }, 2e3), $(".owl_1_part").owlCarousel({
    loop: !0,
    nav: !0,
    dots: !1,
    autoplay: !0,
    autoplayTimeout: 5e3,
    autoplayHoverPause: !0,
    smartSpeed: 1e3,
    items: 1
  }), $(".owl_1_part .owl-nav").attr("id", "owl-nav1"), $(".owl_3_part").owlCarousel({
    loop: !0,
    nav: !0,
    dots: !1,
    autoplay: !0,
    autoplayTimeout: 4e3,
    autoplayHoverPause: !0,
    smartSpeed: 1e3,
    responsive: {
      0: {
        margin: 0,
        items: 1
      },
      575: {
        margin: 20,
        items: 2
      },
      767: {
        margin: 20,
        items: 3
      },
      991: {
        margin: 40,
        items: 3
      }
    }
  }), $(".owl_3_part_2").owlCarousel({
    loop: !0,
    nav: !1,
    dots: !1,
    autoplay: !0,
    autoplayTimeout: 4e3,
    autoplayHoverPause: !0,
    smartSpeed: 1e3,
    margin: 0,
    responsive: {
      0: {
        items: 1
      },
      575: {
        items: 2
      },
      767: {
        items: 3
      },
      991: {
        items: 3,
        loop: !1,
        autoplay: !1
      }
    }
  }), $(".owl_3_part .owl-nav").attr("id", "owl-nav1"), $(".blockQuestion .QandAPart .question").click(function () {
    $(this).parent().hasClass("active") ? $(this).parent().removeClass("active") : ($(".blockQuestion .QandAPart").removeClass("active"), $(this).parent().addClass("active"));
    var e = $(this).offset().top;
    $("html,body").animate({
      scrollTop: e - 100
    }, 300);
  }), $(function () {
    var e = $(window),
      o = e.scrollTop(),
      n = $(".wrapSticky");
    if (n.length > 0) {
      var t = n.position().top;
      e.scroll(function () {
        var a = e.height(),
          i = n.outerHeight(),
          r = e.scrollTop(),
          l = r + a,
          s = n.position().top,
          c = s + i,
          u = Math.abs(a - i),
          d = o - r,
          p = r > o,
          m = a > i;
        m && r > t || !m && r > t + u ? n.addClass("fixed") : !p && r <= t && n.removeClass("fixed");
        var h = s >= r && !p;
        if (c <= l && p) m ? n.css("top", 0) : n.css("top", -u);else if (h) n.css("top", 0);else if (n.hasClass("fixed")) {
          var g = -u,
            f = parseInt(n.css("top"), 10) + d,
            v = r + a >= $(document).height() ? g : f;
          n.css("top", v);
        }
        o = r, p;
      });
    }
  }), n();
  $(".menuFagoAgency_DESK .menuLine_1").height();
  $(window).scroll(function () {
    $("html,body").scrollTop() > 0 ? $(".menuFagoAgency_DESK .menuLine_1 .bgUnderMenuTopDesk").css({
      top: "0px",
      opacity: "1"
    }) : $(".menuFagoAgency_DESK .menuLine_1 .bgUnderMenuTopDesk").css({
      top: "-100%",
      opacity: "0"
    });
  }), $(".menuFagoAgency_DESK .menuLine_1 .menuPart .dropDown").mouseover(function () {
    $(this).find(".blockLevel2").addClass("appear");
  }), $(".menuFagoAgency_DESK .menuLine_1 .menuPart .dropDown").mouseout(function () {
    $(this).find(".blockLevel2").removeClass("appear");
  }), $("body:has(.fixMenuSpecial)").find(".menuFagoAgency_DESK .menuLine_1").addClass("bgColorMenuDesk"), $("body:has(.fixMenuSpecial)").find(".menuFagoAgency_DESK .menuLine_1 .bgUnderMenuTopDesk").remove();
  var r = $(".menuFagoAgency_DESK .menuLine_1").height();
  $(".fixMenuSpecial").css("margin-top", r), $("#contactBtnBlock .phoneBlock .imgPart img").click(function () {
    $("#contactBtnBlock .phoneBlock .leavePhoneForm").fadeIn(600);
  }), $("#contactBtnBlock .phoneBlock .leavePhoneForm .turnOffForm .xItem").click(function () {
    $("#contactBtnBlock .phoneBlock .leavePhoneForm").fadeOut(600);
  }), $("#contactBtnBlock .pageUp .imgPart img").click(function () {
    $("html,body").animate({
      scrollTop: 0
    }, 600);
  }), $(".menuFagoAgency_MOBILE .menuEndPage .smallPart .aTagBars").click(function () {
    $(".menuFagoAgency_MOBILE .blockCateMobile").fadeIn(600);
  }), $(".menuFagoAgency_MOBILE .blockCateMobile .hideBlock .xItem").click(function () {
    $(".menuFagoAgency_MOBILE .blockCateMobile").fadeOut(600);
  }), $(".menuFagoAgency_MOBILE .blockCateMobile .wrapCateParts .catePart.dropMore .faFix.fa-angle-right").click(function () {
    $(this).parent().find(".blockLevel2").fadeIn(600), $(this).css("display", "none"), $(this).parent().find(".fa-angle-down").css("display", "block");
  }), $(".menuFagoAgency_MOBILE .blockCateMobile .wrapCateParts .catePart.dropMore .faFix.fa-angle-down").click(function () {
    $(this).parent().find(".blockLevel2").fadeOut(600), $(this).css("display", "none"), $(this).parent().find(".fa-angle-right").css("display", "block");
  }), setTimeout(function () {
    function e() {
      var e = $(".menuFagoAgency_MOBILE .menuEndPage").innerHeight() + 20;
      $(window).width() <= 767 ? $(".footerEndPage").css("margin-bottom", e) : $(".footerEndPage").css("margin-bottom", "0");
    }
    e(), $(window).resize(function () {
      e();
    });
  }, 1e3), $(".authorFagoAgency .section_1 .owlSlideTabBlock").owlCarousel({
    loop: !1,
    margin: 5,
    nav: !1,
    dots: !1,
    responsive: {
      0: {
        items: 1
      },
      575: {
        items: 2
      },
      757: {
        items: 3
      }
    }
  }), $(".authorFagoAgency .owlSlideTabBlock .text_1").click(function () {
    var e = $(window).width(),
      o = $("#tabIntroduce").offset().top,
      n = 0;
    e >= 992 && (n = $(".menuFagoAgency_DESK .menuLine_1").height()), $("html,body").animate({
      scrollTop: o
    }, 600), $("html,body").animate({
      scrollTop: o - n - 10
    }, 1e3);
  }), $(".authorFagoAgency .owlSlideTabBlock .text_2").click(function () {
    var e = $(window).width(),
      o = $("#tabExperience").offset().top,
      n = 0;
    e >= 992 && (n = $(".menuFagoAgency_DESK .menuLine_1").height()), $("html,body").animate({
      scrollTop: o
    }, 600), $("html,body").animate({
      scrollTop: o - n - 10
    }, 1e3);
  }), $(".authorFagoAgency .owlSlideTabBlock .text_3").click(function () {
    var e = $(window).width(),
      o = $("#tabSecRelatedNews").offset().top,
      n = 0;
    e >= 992 && (n = $(".menuFagoAgency_DESK .menuLine_1").height()), $("html,body").animate({
      scrollTop: o
    }, 600), $("html,body").animate({
      scrollTop: o - n - 20
    }, 1e3);
  }), $("#serviceFagoAgency_7 .section_2").length && $(".downToBangGia").click(function () {
    var e = $("#serviceFagoAgency_7 .section_2").offset().top;
    $("html,body").animate({
      scrollTop: e + 100
    }, 400), $("html,body").animate({
      scrollTop: e - 60
    }, 1e3);
  }), $("#owl-nav1 .owl-prev span").replaceWith("\u2039"), $("#owl-nav1 .owl-next span").replaceWith("\u203a");
}), $(document).ready(function () {
  var e = $(window).width();
  if ($("#backLink").click(function () {
    history.back(1), event.preventDefault();
  }), $(".specialBlock_16").length) {
    var o = document.querySelector(".specialBlock_16 .leftBlock .area_3 .wrapItemDown");
    o.onclick = function () {
      var e = o.parentElement.parentElement.parentElement.parentElement.parentElement.clientHeight;
      $("html,body").animate({
        scrollTop: e
      }, 300), $("html,body").animate({
        scrollTop: e - 60
      }, 600);
    };
  }
  $(".contactCallPopUp").click(function () {
    var e = $(this).attr("dataHref"),
      o = $(this).attr("contactText"),
      n = $(this).attr("idGoogleGet"),
      t = $(".popupAskContact .modal-footer .btnContact");
    t.attr("href", e), t.attr("id", n), t.text(o);
  }), e >= 576 && setTimeout(function () {
    $(".contactCallPopUpPhone").attr("data-target", "");
  }, 500), $(".contactCallPopUpShowTwoElement").attr("data-target", "#popupWhenClickContactShowTwoElement"), $(".contactCallPopUpShowTwoElement").attr("data-toggle", "modal"), $(".contactCallPopUpShowTwoElement").click(function () {
    var e = $(this).attr("chooseContact");
    $("#popupWhenClickContactShowTwoElement").find(".chooseContact").fadeOut(0), $("#popupWhenClickContactShowTwoElement .chooseContact." + e).fadeIn(0), console.log(e);
  }), $(window).scroll(function () {
    $("html,body").scrollTop() > 100 ? $("#pageUpBlock .pageUp").removeClass("hidePageUp") : $("#pageUpBlock .pageUp").addClass("hidePageUp");
  }), $("#pageUpBlock .pageUp").click(function () {
    $("html,body").animate({
      scrollTop: 0
    }, 600);
  });
}), $(document).ready(function () {
  $(".data_contents").length && fixDataContents();
}), $(function () {
  $("form.order-form").each((e, o) => {
    $(o).validate({
      onfocusout: !1,
      onkeyup: !1,
      onclick: !1,
      rules: {
        customer_name: {
          required: !0
        },
        customer_phone: {
          required: !0,
          phoneNumber: !0
        },
        customer_email: {
          required: !0,
          email: !0
        },
        "shipping_address[address]": {
          required: !0
        },
        "shipping_address[province]": {
          required: !0
        },
        "shipping_address[district]": {
          required: !0
        },
        "shipping_address[ward]": {
          required: !0
        },
        "invoice_data[company_name]": {
          required: !0
        },
        "invoice_data[tax_number]": {
          required: !0
        },
        "invoice_data[address]": {
          required: !0
        }
      },
      messages: {}
    });
  }), $("form.order-form .submit-order").click(() => {
    $("form.order-form").submit();
  }), $(".submit-direct-order.ksch").click(() => {
    const e = $("form.product-form").find(".typePet.active"),
      o = e.attr("typeroom"),
      n = e.text(),
      t = e.attr("priceroom"),
      a = $("form.product-form");
    a.find('input[name="variants[room_type]"]').val(o), a.find('input[name="variants[pet_type]"]').val(n), a.find('input[name="price"]').val(t), a.submit();
  }), $(".submit-direct-order.spa-cho-meo").click(() => {
    const e = $("form.product-form"),
      o = e.find(".serviceTab.active").text(),
      n = e.find(".serviceBlock.active").find(".typePet.active"),
      t = n.text(),
      a = n.attr("priceroom");
    e.find('input[name="variants[room_type]"]').val(o), e.find('input[name="variants[pet_type]"]').val(t), e.find('input[name="price"]').val(a), e.submit();
  }), $(".submit-direct-order.product").click(() => {
    $("form.product-form").submit();
  }), $("form.order-form").on("onValid", function () {
    $(this).parent().find(".buttonload_booking").css("display", "block"), $(this).parent().find(".submit-order").css("display", "none");
  }), $("form.order-form").on("onSuccess", function () {
    $(this).parent().find(".buttonload_booking").css("display", "none"), $(this).parent().find(".submit-order").css("display", "block");
  }), $("form.order-form").on("onError", function () {
    $(this).parent().find(".buttonload_booking").css("display", "none"), $(this).parent().find(".submit-order").css("display", "block");
  });
}), $(function () {
  setCustomValidationMessages();
}), $(document).ready(function () {
  function e() {
    var e,
      o,
      n,
      t = "<ul>";
    $(".getTableOfContentBlock h3, .getTableOfContentBlock h2 ").each(function (a, i) {
      o = $(this), n = o.text().trim();
      var r = removeAccents(n);
      o.attr("id", r), "H3" == i.tagName ? e = "<li class='sub_data'><a href='" + location.origin + location.pathname + "#" + r + "'>" + n + "</a></li>" : "H2" == i.tagName && (e = "<li  class='data'><a href='" + location.origin + location.pathname + "#" + r + "'>" + n + "</a></li>"), t += e;
    }), null == e ? t = "" : t += "</ul>", $(".bookmark-list").prepend(t);
  }
  function o() {
    $(".tableOfContent .clickToggle").click(function () {
      var e = $(this).parent().parent();
      e.hasClass("appearContent") ? e.removeClass("appearContent") : e.addClass("appearContent");
    });
  }
  e(), o();
});